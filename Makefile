all:
	$(CXX) -c main.cpp -O2 --std=c++11 -fopenmp -ltbb
	$(CXX) -c pngio.cpp -O2 --std=c++11 -fopenmp -ltbb
	$(CXX) -o kmeansDithering main.o pngio.o -lpng -lz -fopenmp -ltbb

macos:
	icc -c main.cpp -O2 -std=c++11 -openmp -ltbb
	icc -c pngio.cpp -O2 -std=c++11 -openmp -ltbb
	icc -o kmeansDithering main.o pngio.o -lpng -lz -openmp -ltbb  -std=c++11
	
debug:
	./kmeansDithering Lenna.png -serial	

run:
	./kmeansDithering Lenna.png -serial