/* 
 * File:   main.cpp
 * Author: temp3rr0r
 *
 * Created on April 1, 2015, 11:21 PM
 */

#include <cstdlib>
#include <iostream>
#include <map>
#include <cmath>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

#include "settings.h"
#include "pngio.h"
#include "util.h"
#include "util_openmp.h"
#include "util_tbb.h"

using namespace std;
using namespace tbb;

/**
 * Main execution point. Should be able to select between serial, or openMP or
 * TBB execution
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char * argv[]) {
    
    int executionType = -1;
    int maxThreads = MAX_THREADS;
    int maxIterations = ITERATIONS;
    int kernelCenters = KERNEL_CENTERS;
    double totalTime = 0.0;
    
    if (argc < 3 ) {
        throw runtime_error("USAGE: " + string(argv[0]) + " filepath -(omp or serial or tbb)\n"
                "i.e " + string(argv[0]) + " Lenna.png -omp");
    }
    
    string fileName = argv[1];
    string type = argv[2];
    
    if (type.compare("-serial") == 0) {
        executionType = 0;
        cout << "=========== Serial execution ===========" << endl;
    } else if (type.compare("-omp") == 0) {
        executionType = 1;
        cout << "===== Parallel execution with OpenMP ====" << endl;
        cout << "Number of threads: " << maxThreads << endl;
    } else if (type.compare("-tbb") == 0) {
        executionType = 2;        
        cout << "=== Parallel execution with Intel TBB ===" << endl;
        cout << "Number of threads: " << maxThreads << endl;
    } else {
        throw runtime_error("USAGE: " + string(argv[0]) + " filepath -(omp or serial or tbb)\n"
                "i.e " + string(argv[0]) + " Lenna.png -omp");
    }
    
    string platform;
    
    cout << "Number of iterations: " << maxIterations << endl;
    cout << "Number of kernels: " << kernelCenters << endl;
    
    cout << "Applying K-Means clustering and dithering on " << string(argv[1]) <<  "..." << endl;
    
    ImageRGB inputImage = readpng(fileName);
    
    switch (executionType) {
        case 0: // Serial
        {    
            platform = "Serial";
            struct timeval  tv1, tv2;            
            
            for (int iterationCount = 0; iterationCount < maxIterations; iterationCount++) {
                
                // Start the timer
                gettimeofday(&tv1, NULL);
                
                ColourCount colourCount = countImageDistinctColours(inputImage, kernelCenters);

                // TODO: change iteration count
                for (int i = 0; i < 1; i++) {
                    if (!isConverged(colourCount)) {
                        colourCount.PreviousCluster = colourCount.CurrentCluster;
                        iterate(inputImage, colourCount);
                    }
                }

                // Segment the image based on the cluster
                inputImage = segmentImage(inputImage, colourCount);

                // Dither image
                inputImage = ditherImage(inputImage);
                
                // End the timer
                //end = clock();
                gettimeofday(&tv2, NULL);
                
                // Add the time for the current iteration
                //totalTime += (end - start) / (double)CLOCKS_PER_SEC;
                totalTime += (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
         (double) (tv2.tv_sec - tv1.tv_sec);
            }
                        
            totalTime /= maxIterations;
        
            break;
        }
        case 1: // OpenMP
        {
            platform = "OpenMP";
            // Enforce the number of maximum threads
            omp_set_num_threads(maxThreads);
            
            struct timeval  tv1, tv2;
            
            for (int iterationCount = 0; iterationCount < maxIterations; iterationCount++) {
                
                // Start the timer
                //start = omp_get_wtime();
                gettimeofday(&tv1, NULL);
                
                ColourCount colourCount = countImageDistinctColoursOmp(inputImage, kernelCenters);

                // TODO: change iteration count
                for (int i = 0; i < 1; i++) {
                    if (!isConverged(colourCount)) {
                        colourCount.PreviousCluster = colourCount.CurrentCluster;
                        iterateOmp(inputImage, colourCount);
                    }
                }

                // Segment the image based on the cluster
                inputImage = segmentImageOmp(inputImage, colourCount);

                // Dither image
                inputImage = ditherImageOmp(inputImage);
                
                // End the timer
                //end = omp_get_wtime();
                gettimeofday(&tv2, NULL);
                
                // Add the time for the current iteration
                //totalTime += (double)(end - start);
                totalTime += (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
         (double) (tv2.tv_sec - tv1.tv_sec);
            }
                        
            totalTime /= maxIterations;
            
             break;
        }
        case 2: // Thread Building Blocks
        {
            platform = "TBB";
            // Enforce the number of maximum threads
            tbb::task_scheduler_init init(maxThreads);
            
            if (DEBUG)
                cout << "TBB version: " << TBB_runtime_interface_version() << endl;;
            
            //tbb::tick_count startTbb = tbb::tick_count::now();
            //tbb::tick_count endTbb;
            struct timeval  tv1, tv2;
            
            for (int iterationCount = 0; iterationCount < maxIterations; iterationCount++) {
                
                // Start the timer
                gettimeofday(&tv1, NULL);
                
                ColourCount colourCount = countImageDistinctColoursTbb(inputImage, kernelCenters);
                //ColourCountTbb colourCountTbb = countImageDistinctColoursTbb(inputImage, KERNEL_CENTERS);

                // TODO: change iteration count
                for (int i = 0; i < 1; i++) {
                    if (!isConverged(colourCount)) {
                        colourCount.PreviousCluster = colourCount.CurrentCluster;
                        iterateTbb(inputImage, colourCount);
                    }
                }

                // Segment the image based on the cluster
                inputImage = segmentImageTbb(inputImage, colourCount);

                // Dither image
                inputImage = ditherImageTbb(inputImage);

                // End the timer
                //endTbb = tbb::tick_count::now();
                gettimeofday(&tv2, NULL);
                
                // Add the time for the current iteration
                //totalTime += (double)(endTbb - startTbb).seconds();
                totalTime += (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
         (double) (tv2.tv_sec - tv1.tv_sec);
            }
                        
            totalTime /= maxIterations;
            
             break;    
        }
    }
    
    writepng("dithered_" + fileName, inputImage);
    cout << "Done in " << totalTime << " seconds." << endl;
    
    if (LOG) {        
        FILE* pFile;
        string fileName = platform;
        fileName.append("K");
        fileName.append(to_string(kernelCenters));
        fileName.append(argv[1]);
        fileName.append(".csv");
                
        pFile = fopen(fileName.c_str(), "a+");
        
        //if( access(pFile, F_OK) == -1 ) {
        struct stat buffer;
        if (stat (fileName.c_str(), &buffer) != 0) {
            // Image,Algorithm,Threads,Kernels,Iterations,Time
            fprintf(pFile, "ImageName,Platform,Threads,Kernels,Iterations,Time\n");
        }
        
        fprintf(pFile, "%s,", argv[1]);
        fprintf(pFile, "%s,", platform.c_str());
        fprintf(pFile, "%d,", maxThreads);
        fprintf(pFile, "%d,", KERNEL_CENTERS);
        fprintf(pFile, "%d,", maxIterations);
        fprintf(pFile, "%f\n", totalTime);
        fclose(pFile);        
    }
 

    return 0;
}