/* 
 * File:   util.h
 * Author: temp3rr0r
 *
 * Created on June 14, 2015, 12:08 AM
 */

#ifndef UTIL_H
#define	UTIL_H

#include <map>
#include <cmath>

#include "settings.h"

using namespace std;

uint32_t getRGBHex(uint8_t r, uint8_t g, uint8_t b) {
    return (r  << 16) + (g << 8) + b;
}

uint8_t getRedColour(uint32_t rgbHex) {
    return rgbHex >> 16 & 0xFF;
}

uint8_t getGreenColour(uint32_t rgbHex) {
    return rgbHex >> 8 & 0xFF;
}

uint8_t getBlueColour(uint32_t rgbHex) {
    return rgbHex & 0xFF;
}

/**
 * Stores useful information. Count of the most common colours, the previous
 * kernel cluster centers, the current kernel cluster centers and the allocation
 * of each pixel to each of the current cluster centers.
 */
struct ColourCount
{
    map<uint32_t, uint32_t> CountMap;
    map<uint32_t, uint32_t, std::greater<int>> CountMapReversed; // reverse for fast access
    map<uint32_t, std::array<float,3>> PreviousCluster;
    map<uint32_t, std::array<float,3>> CurrentCluster;
    map<uint32_t, vector<uint32_t>> PixelClusterAllocation;
    map<uint32_t, uint32_t> ClusterColours;
};

/**
 * Given an input image and a number of cluster centers, the most common colours
 * are being stored in the ColourCount struct.
 */
ColourCount countImageDistinctColours(const ImageRGB& inputImage, int clusterCenters) {
               
    ColourCount colourCount;
    
    try {
        ImageRGB imgMixedChannels;
        imgMixedChannels.width = inputImage.width;
        imgMixedChannels.height = inputImage.height;

        for (size_t i = 0; i < inputImage.height; ++i) {
            for (size_t j = 0; j < inputImage.width; ++j) {
                
                const uint8_t r = get<0>(inputImage.data[i * inputImage.width + j]);
                const uint8_t g = get<1>(inputImage.data[i * inputImage.width + j]);
                const uint8_t b = get<2>(inputImage.data[i * inputImage.width + j]);
               
                const uint32_t hexColour = getRGBHex(r, g, b);                
                
                // Found the colour, increase count
                if (colourCount.CountMap.find(hexColour)!= colourCount.CountMap.end()) {
                    colourCount.CountMap[hexColour]++;
                    
                } else {
                    colourCount.CountMap.insert(std::make_pair(hexColour, 1));
                }
            }
        }
                
        // Populate the reversed map HEX
        for (const auto& currentColour : colourCount.CountMap) {
            colourCount.CountMapReversed.insert(std::make_pair(currentColour.second,
                    currentColour.first)); 
        }

        // Populate top kernel colours
        int topColoursCount = 0;
        for (const auto& currentColour : colourCount.CountMapReversed) {            
            if (++topColoursCount <= clusterCenters) {
                
                std::array<float,3> centroids = { (float)getRedColour(currentColour.second),
                    (float)getGreenColour(currentColour.second),
                    (float)getBlueColour(currentColour.second)};
                
                colourCount.PreviousCluster.insert(std::make_pair(currentColour.second, centroids));
                colourCount.CurrentCluster.insert(std::make_pair(currentColour.second, centroids));
                
                if (DEBUG)
                    cout << "Centroids: " << get<1>(centroids) << endl;
            } else
                break;            
        }

    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }
    
    return colourCount;
}

/**
 * For a given pixel and the current list of cluster centers, the minimum distance
 * of that pixel is being found. This pixel is then being assigned/allocated to
 * that cluster center.
 */
void allocateToCluster(uint32_t pixel, map<uint32_t, std::array<float,3>>& clusterCenters, 
        map<uint32_t, vector<uint32_t>> &pixelClusterAllocation) {   
    try {
        // TODO: Parallelize here        
        map<float, uint32_t> distancesMap;
        
        const int pixelRed = getRedColour(pixel);
        const int pixelGreen = getGreenColour(pixel);
        const int pixelBlue = getBlueColour(pixel);        
        
        // find all distances of the pixel from all the cluster centers
        for (const auto& it : clusterCenters) {
            uint32_t currentKernel = it.first;
                        
            const int currentKernelRed = getRedColour(currentKernel);
            const int currentKernelGreen = getGreenColour(currentKernel);
            const int currentKernelBlue = getBlueColour(currentKernel);
            
            // calculate distances
            float currentDistance = (float)sqrt(pow((currentKernelRed - pixelRed), 2) + 
                pow((currentKernelGreen - pixelGreen), 2) + 
                pow((currentKernelBlue - pixelBlue), 2));
                        
            distancesMap.insert(std::make_pair(currentDistance, currentKernel));
        }
   
        // get the minimum distance - The [0] element of the map has
        // the smallest distance by default, due to the nature of map data structures in c++ 
        uint32_t minDistanceKernel;
        for (const auto& currentColour : distancesMap) {
            minDistanceKernel = currentColour.second;
            break;            
        }

        // allocate that pixel to the closest cluster center        
        if (pixelClusterAllocation.find(minDistanceKernel)!= pixelClusterAllocation.end()) {
            pixelClusterAllocation[minDistanceKernel].push_back(pixel);
        } else {
            vector<uint32_t> newPixelVector;
            newPixelVector.push_back(pixel);
            pixelClusterAllocation.insert(std::make_pair(minDistanceKernel, newPixelVector));
        }        
        
    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }    
}

/*
 * Calculate cluster centroids
 */
void CalculateClusterCentroids(ColourCount& colourCount) {     
        
    for (const auto& currentCluster : colourCount.CurrentCluster) {        
                
        vector<uint32_t> colourList = colourCount.PixelClusterAllocation[currentCluster.first];
        
        float centroidRed = 0.0;
        float centroidGreen = 0.0;
        float centroidBlue = 0.0;
        
        for (std::vector<uint32_t>::iterator it = colourList.begin() ; it != colourList.end(); ++it) {
            
            uint32_t currentPixel = *it;

            std::array<float,3> currentClusterArray = colourCount.CurrentCluster.find(currentPixel)->second;
            
            centroidRed += get<0>(currentClusterArray);
            centroidGreen += get<1>(currentClusterArray);
            centroidBlue += get<2>(currentClusterArray);
                        
            map<uint32_t, uint32_t>::const_iterator it2 = colourCount.ClusterColours.find(currentPixel);            
            if (it2 == colourCount.ClusterColours.end()) {
                // Added to the cluster colours                
                colourCount.ClusterColours.insert(std::make_pair(currentPixel, getRGBHex(
                    centroidRed, centroidGreen, centroidBlue)));                        
            }
            
        }
        
        float count = colourList.size() + 1;
        
        // Average for the new centroid
        std::array<float,3> currentClusterArray = colourCount.CurrentCluster.find(currentCluster.first)->second;
        std::array<float,3> newCentroid = {(get<0>(currentClusterArray) + centroidRed) / count,
            (get<1>(currentClusterArray) + centroidGreen) / count,
            (get<2>(currentClusterArray) + centroidBlue) / count};
        
        if (DEBUG) {
            cout << "get<1>(currentClusterArray): " << get<1>(currentClusterArray)
                    << " centroid green: " << centroidGreen
                    << " count: " << count
                    << " final value: " << (get<1>(currentClusterArray) + centroidGreen) / count << endl;        
        }
        
        if (DEBUG)
            cout << "colourCount.CurrentCluster.find(currentCluster.first)-> second(BEFORE): "
                  << get<0>(colourCount.CurrentCluster.find(currentCluster.first)-> second) << endl;
        
        // Store the new centroid value
        colourCount.CurrentCluster.find(currentCluster.first)-> second = newCentroid;
        
        if (DEBUG)
            cout << "colourCount.CurrentCluster.find(currentCluster.first)-> second(AFTER): "
                  << get<0>(colourCount.CurrentCluster.find(currentCluster.first)-> second) << endl;
        
        if (DEBUG)
            cout << "colourCount.CurrentCluster.find(currentCluster.first)-> second(SHOULD): "
                  << (get<0>(newCentroid)) << endl;
    }
}

/**
 * Get Random points/cluster centers
 * @return 
 */
std::vector<std::array<uint32_t,2>> getRandomClusterCenters(int clusterSize, int maxHeight, int maxWidth) {
    
    vector<std::array<uint32_t,2>> randomPointsVector;    
    uint32_t x = 0;
    uint32_t y = 0;
    
    // Initialize random seed
    srand (time(NULL));    

    // Generate a number between the pixel dimensions
    for (int i = 0; i < clusterSize; i++) {        
        x = rand() % maxHeight;
        y = rand() % maxWidth;
        randomPointsVector.push_back({x, y});
    }
    
    return randomPointsVector;
}

/**
 * Check if the result has converged by comparing the previous with the current
 * cluster.
 */
bool isConverged(ColourCount colourCount) {
    bool match = false;
        
    if (colourCount.PreviousCluster == colourCount.CurrentCluster)
        match = true;
    
    if (!match)
        colourCount.PreviousCluster = colourCount.CurrentCluster;
    
    return match;
}

ImageRGB segmentImage(const ImageRGB& inputImage, ColourCount& colourCount) {
    try {
        ImageRGB imgMixedChannels;
        imgMixedChannels.width = inputImage.width;
        imgMixedChannels.height = inputImage.height;

        for (size_t i = 0; i < inputImage.height; ++i) {
            for (size_t j = 0; j < inputImage.width; ++j) {
                const uint8_t r = get<0>(inputImage.data[i * inputImage.width + j]);
                const uint8_t g = get<1>(inputImage.data[i * inputImage.width + j]);
                const uint8_t b = get<2>(inputImage.data[i * inputImage.width + j]);
                
                const uint32_t hexColour = getRGBHex(r, g, b); 
                
                // look up the float of the latest cluster
                //0. uint32_t newColour = colourCount.ClusterColours[hexColour];
                //map<uint32_t, vector<uint32_t>> PixelClusterAllocation;
                // 1. colourCount.PixelClusterAllocation
                // 2. CurrentCluster
                // 3. uint32_t newColour = getRGBHex(get<0>(colourCount.CurrentCluster[hexColour]), 
                //        get<1>(colourCount.CurrentCluster[hexColour]), get<2>(colourCount.CurrentCluster[hexColour]));
                
                //map<uint32_t, std::array<float,3>> CurrentCluster;
                
                uint32_t newColour = colourCount.ClusterColours[hexColour];
                
                // find the closest kernel of the current colour and replace it...
                int pixelRed = getRedColour(hexColour);
                int pixelGreen = getGreenColour(hexColour);
                int pixelBlue = getBlueColour(hexColour);  
                map<float, uint32_t> distancesMap;      

                // find all distances of the pixel from all the cluster centers
                for (const auto& it : colourCount.CurrentCluster) {
                    uint32_t currentKernel = it.first;

                    const int currentKernelRed = getRedColour(currentKernel);
                    const int currentKernelGreen = getGreenColour(currentKernel);
                    const int currentKernelBlue = getBlueColour(currentKernel);

                    // calculate distances
                    float currentDistance = (float)sqrt(pow((currentKernelRed - pixelRed), 2) + 
                        pow((currentKernelGreen - pixelGreen), 2) + 
                        pow((currentKernelBlue - pixelBlue), 2));

                    distancesMap.insert(std::make_pair(currentDistance, currentKernel));
                }

                // get the minimum distance - The [0] element of the map has
                // the smallest distance by default, due to the nature of map data structures in c++ 
                uint32_t minDistanceKernel;
                for (const auto& currentColour : distancesMap) {
                    minDistanceKernel = currentColour.second;
                    break;            
                }
                newColour = minDistanceKernel;
                
                if (DEBUG) {
                    cout << "colourCount.CurrentCluster size: " << colourCount.CurrentCluster.size() << endl;
                    cout << "colourCount.ClusterColours size: " << colourCount.ClusterColours.size() << endl;
                    cout << "Hexcolour: " << hexColour << " newColour: " << newColour << endl;
                }                    
                
                // replace the pixel colour                
                imgMixedChannels.data.push_back({(uint8_t)getRedColour(newColour),
                        (uint8_t)getGreenColour(newColour), (uint8_t)getBlueColour(newColour)});                    
            }
        }
                
        return imgMixedChannels;
        
    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }
}

/**
 * Segments the colour range from 0-255 to values: 0, 64, 128, 192, 255
 */
uint8_t getColour4bit(uint8_t colour) {

    uint8_t returningColour;
    
    if (colour < 64)
        returningColour = 0;
    else if (colour >= 64 && colour < 128)
        returningColour = 64;
    else if (colour >= 192)
        returningColour = 255;
    else
        returningColour = 128;
    
    return returningColour;
}

/**
 * Segments the colour range from 0-255 to values: 0, 255
 */
uint8_t getColour1bit(uint8_t colour) {  
    return (colour > 127)? 255 : 0;
}

/**
 * Part of dithering, finds the closest colour
 */
uint32_t findClosestPaletteColour(uint8_t r, uint8_t g, uint8_t b) {
             
    // find the closest kernel of the current colour and replace it...
    uint8_t pixelRed = getColour4bit(r);
    uint8_t pixelGreen = getColour4bit(g);
    uint8_t pixelBlue = getColour4bit(b);

    return getRGBHex(pixelRed, pixelGreen, pixelBlue);   
}

/**
 * Returns a dithered image
 */
ImageRGB ditherImage(const ImageRGB& inputImage) {
    ImageRGB imgMixedChannels = inputImage;
    try {
        imgMixedChannels.width = inputImage.width;
        imgMixedChannels.height = inputImage.height;
        
        for (size_t i = 0; i < imgMixedChannels.height; ++i) {
            for (size_t j = 0; j < imgMixedChannels.width; ++j) {
                
                const uint8_t r = get<0>(imgMixedChannels.data[i * imgMixedChannels.width + j]);
                const uint8_t g = get<1>(imgMixedChannels.data[i * imgMixedChannels.width + j]);
                const uint8_t b = get<2>(imgMixedChannels.data[i * imgMixedChannels.width + j]);
                
                const uint32_t hexColour = getRGBHex(r, g, b);                
                const uint32_t newColour = findClosestPaletteColour(r, g, b);
                              
                const uint32_t quantizationErrorRed = r - getRedColour(newColour);
                const uint32_t quantizationErrorGreen = g - getGreenColour(newColour);
                const uint32_t quantizationErrorBlue = b - getBlueColour(newColour);
                                
                // replace the current pixel colour                
                imgMixedChannels.data[i * imgMixedChannels.width + j] = {(uint8_t)getRedColour(newColour),
                        (uint8_t)getGreenColour(newColour), (uint8_t)getBlueColour(newColour)};    
                
                // pixel[x+1][y  ] := pixel[x+1][y  ] + quant_error * 7/16
                if ((i + 1) < imgMixedChannels.height) {
                    uint8_t rE = get<0>(imgMixedChannels.data[(i + 1) * imgMixedChannels.width + j]);
                    uint8_t gE = get<1>(imgMixedChannels.data[(i + 1) * imgMixedChannels.width + j]);
                    uint8_t bE = get<2>(imgMixedChannels.data[(i + 1) * imgMixedChannels.width + j]);
                    
                    imgMixedChannels.data[(i + 1) * imgMixedChannels.width + j] =
                        { (uint8_t)(rE + ((7 * quantizationErrorRed) >> 4)), 
                        (uint8_t)(gE + ((7 * quantizationErrorGreen) >> 4)),
                        (uint8_t)(bE + ((7 * quantizationErrorBlue) >> 4)) };
                }
                                
                // pixel[x-1][y+1] := pixel[x-1][y+1] + quant_error * 3/16
                if ((i - 1) >= 0 && (j + 1) < imgMixedChannels.width &&
                        ((i - 1) * imgMixedChannels.width + (j + 1)) <
                        (imgMixedChannels.width * imgMixedChannels.height) &&
                        ((i - 1) * imgMixedChannels.width + (j + 1)) > 0) {
                    uint8_t rE = get<0>(imgMixedChannels.data[(i - 1) * imgMixedChannels.width + (j + 1)]);
                    uint8_t gE = get<1>(imgMixedChannels.data[(i - 1) * imgMixedChannels.width + (j + 1)]);
                    uint8_t bE = get<2>(imgMixedChannels.data[(i - 1) * imgMixedChannels.width + (j + 1)]);
                    
                    imgMixedChannels.data[(i - 1) * imgMixedChannels.width + (j + 1)] =
                        { (uint8_t)(rE + ((3 * quantizationErrorRed) >> 4)), 
                        (uint8_t)(gE + ((3 * quantizationErrorGreen) >> 4)),
                        (uint8_t)(bE + ((3 * quantizationErrorBlue) >> 4)) };
                }
                
                // pixel[x  ][y+1] := pixel[x  ][y+1] + quant_error * 5/16
                if ((j + 1) < imgMixedChannels.width) {
                    uint8_t rE = get<0>(imgMixedChannels.data[i * imgMixedChannels.width + (j + 1)]);
                    uint8_t gE = get<1>(imgMixedChannels.data[i * imgMixedChannels.width + (j + 1)]);
                    uint8_t bE = get<2>(imgMixedChannels.data[i * imgMixedChannels.width + (j + 1)]);
                    
                    imgMixedChannels.data[i * imgMixedChannels.width + (j + 1)] =
                        { (uint8_t)(rE + ((5 * quantizationErrorRed) >> 4)), 
                        (uint8_t)(gE + ((5 * quantizationErrorGreen) >> 4)),
                        (uint8_t)(bE + ((5 * quantizationErrorBlue) >> 4)) };
                }
                
                // pixel[x+1][y+1] := pixel[x+1][y+1] + quant_error * 1/16
                if ((i + 1) < imgMixedChannels.height && (j + 1) < imgMixedChannels.width) {
                    uint8_t rE = get<0>(imgMixedChannels.data[(i + 1) * imgMixedChannels.width + (j + 1)]);
                    uint8_t gE = get<1>(imgMixedChannels.data[(i + 1) * imgMixedChannels.width + (j + 1)]);
                    uint8_t bE = get<2>(imgMixedChannels.data[(i + 1) * imgMixedChannels.width + (j + 1)]);
                    
                    imgMixedChannels.data[(i + 1) * imgMixedChannels.width + (j + 1)] =
                        { (uint8_t)(rE + ((1 * quantizationErrorRed) >> 4)), 
                        (uint8_t)(gE + ((1 * quantizationErrorGreen) >> 4)),
                        (uint8_t)(bE + ((1 * quantizationErrorBlue) >> 4)) };
                }                             
            }
        }               
        
    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }
    return imgMixedChannels;
}

/**
 * Perfoms a k-means iteration in the rgb colour space
 */
void iterate(const ImageRGB& inputImage, ColourCount& colourCount) {
    
    try {
        
        // for each pixel in the image        
        for (size_t i = 0; i < inputImage.height; ++i) {
            for (size_t j = 0; j < inputImage.width; ++j) {
                
                const uint8_t r = get<0>(inputImage.data[i * inputImage.width + j]);
                const uint8_t g = get<1>(inputImage.data[i * inputImage.width + j]);
                const uint8_t b = get<2>(inputImage.data[i * inputImage.width + j]);
               
                const uint32_t hexColour = getRGBHex(r, g, b);                                

                // Allocate pixel to cluster
                allocateToCluster(hexColour, colourCount.CurrentCluster, colourCount.PixelClusterAllocation);
            }
        }
        
        // Calculate cluster centroids
        CalculateClusterCentroids(colourCount);

    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }    
}

/**
 * 
 * @param inputImage
 * @param outputFileName
 */
void mixChannelsSaveImage(const ImageRGB& inputImage, string outputFileName) {
    try {
        ImageRGB imgMixedChannels;
        imgMixedChannels.width = inputImage.width;
        imgMixedChannels.height = inputImage.height;
        
        vector<array<uint32_t,2>> randomClustersXY = getRandomClusterCenters(KERNEL_CENTERS,
                inputImage.width, inputImage.height);

        for (size_t i = 0; i < inputImage.height; ++i) {
            for (size_t j = 0; j < inputImage.width; ++j) {
                const uint8_t r = get<0>(inputImage.data[i * inputImage.width + j]);
                const uint8_t g = get<1>(inputImage.data[i * inputImage.width + j]);
                const uint8_t b = get<2>(inputImage.data[i * inputImage.width + j]);
                
                bool found = false;
                for (int k = 0; k < KERNEL_CENTERS; k++) {
                    uint32_t x = get<0>(randomClustersXY[k]);
                    uint32_t y = get<1>(randomClustersXY[k]);
                    
                    if (x == i && y == j) {
                        imgMixedChannels.data.push_back({0, 0, 0});
                        found = true;
                        break;
                    }
                }

                if (i < 10 && j < 10 && !found) {                    
                    //Test, paint black some points
                    imgMixedChannels.data.push_back({0, 0, 0});
                } else if (!found) {
                    imgMixedChannels.data.push_back({b,b,g});
                }                    
            }
        }
        
        writepng("new_" + outputFileName, imgMixedChannels);             
        
    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }
}

#endif /* UTIL_H */