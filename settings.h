/* 
 * File:   settings.h
 * Author: temp3rr0r
 *
 * Created on June 20, 2015, 3:40 AM
 */

#ifndef SETTINGS_H
#define	SETTINGS_H

#define DEBUG 0
#define KERNEL_CENTERS 16
#define MAX_THREADS 4
#define ITERATIONS 20
#define LOG 0

#endif	/* SETTINGS_H */

