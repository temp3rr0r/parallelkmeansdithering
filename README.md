# README #
Parallel k-means clustering and dithering for color quantization

Design and implement a parallel version of Lloyd’s k-means clustering algorithm for finding clusters in a collection of observations. In the specific case of this assignment the observations are(R, G, B) color values of pixels in an image. Each of the color components is an 8-bit integer.