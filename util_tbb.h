/* 
 * File:   util_tbb.h
 * Author: temp3rr0r
 *
 * Created on June 21, 2015, 4:58 PM
 */

#ifndef UTIL_TBB_H
#define	UTIL_TBB_H

#include <map>
#include <cmath>
#include "tbb/tbb.h"
#include "tbb/parallel_for.h"
#include "tbb/tick_count.h"
#include "tbb/concurrent_hash_map.h"
#include "tbb/tbb_stddef.h"

#include "settings.h"

using namespace std;
using namespace tbb;

struct ColourCountTbb
{
    concurrent_hash_map<uint32_t, uint32_t> CountMap;
    concurrent_hash_map<uint32_t, uint32_t> CountMapReversed; // reverse for fast access
    concurrent_hash_map<uint32_t, std::array<float,3>> PreviousCluster;
    concurrent_hash_map<uint32_t, std::array<float,3>> CurrentCluster;
    concurrent_hash_map<uint32_t, concurrent_vector<uint32_t>> PixelClusterAllocation;
    concurrent_hash_map<uint32_t, uint32_t> ClusterColours;
};

/**
 * Given an input image and a number of cluster centers, the most common colours
 * are being stored in the ColourCount struct.
 */
ColourCount countImageDistinctColoursTbb(const ImageRGB& inputImage, int clusterCenters) {
               
    ColourCount colourCount;
    
    try {
        ImageRGB imgMixedChannels;
        imgMixedChannels.width = inputImage.width;
        imgMixedChannels.height = inputImage.height;

        const int height = imgMixedChannels.height;
        const int width = imgMixedChannels.width;
        
        parallel_for(0, height, [&](size_t i) {
            for (size_t j = 0; j < inputImage.width; ++j) {
                
                const uint8_t r = get<0>(inputImage.data[i * width + j]);
                const uint8_t g = get<1>(inputImage.data[i * width + j]);
                const uint8_t b = get<2>(inputImage.data[i * width + j]);
               
                const uint32_t hexColour = getRGBHex(r, g, b);                
                
                // Found the colour, increase count
                if (colourCount.CountMap.find(hexColour)!= colourCount.CountMap.end()) {
                    colourCount.CountMap[hexColour]++;
                    
                } else {
                    colourCount.CountMap.insert(std::make_pair(hexColour, 1));
                }
            }
        });
                
        // Populate the reversed map HEX
        for (const auto& currentColour : colourCount.CountMap) {
            colourCount.CountMapReversed.insert(std::make_pair(currentColour.second,
                    currentColour.first)); 
        }

        // Populate top kernel colours
        int topColoursCount = 0;
        for (const auto& currentColour : colourCount.CountMapReversed) {            
            if (++topColoursCount <= clusterCenters) {
                
                std::array<float,3> centroids = { (float)getRedColour(currentColour.second),
                    (float)getGreenColour(currentColour.second),
                    (float)getBlueColour(currentColour.second)};
                
                colourCount.PreviousCluster.insert(std::make_pair(currentColour.second, centroids));
                colourCount.CurrentCluster.insert(std::make_pair(currentColour.second, centroids));
                
                if (DEBUG)
                    cout << "Centroids: " << get<1>(centroids) << endl;
            } else
                break;            
        }

    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }
    
    return colourCount;
}


ColourCountTbb countImageDistinctColoursTbb2(const ImageRGB& inputImage, int clusterCenters) {
               
    ColourCountTbb colourCount;
    
    try {
        ImageRGB imgMixedChannels;
        imgMixedChannels.width = inputImage.width;
        imgMixedChannels.height = inputImage.height;

        const int height = imgMixedChannels.height;
        const int width = imgMixedChannels.width;
        
        parallel_for(0, height, [&](size_t i) {
            for (size_t j = 0; j < inputImage.width; ++j) {
                
                const uint8_t r = get<0>(inputImage.data[i * width + j]);
                const uint8_t g = get<1>(inputImage.data[i * width + j]);
                const uint8_t b = get<2>(inputImage.data[i * width + j]);
               
                const uint32_t hexColour = getRGBHex(r, g, b);                
                
                concurrent_hash_map<uint32_t, uint32_t>::accessor a;
                
                if (colourCount.CountMap.find(a, hexColour)) {                    
                    colourCount.CountMap.insert(std::make_pair(hexColour, ++(a->second)));                    
                } else {
                    colourCount.CountMap.insert(std::make_pair(hexColour, 1));
                }
            }
        });
                
        // Populate the reversed map HEX
        for (const auto& currentColour : colourCount.CountMap) {
            colourCount.CountMapReversed.insert(std::make_pair(currentColour.second,
                    currentColour.first)); 
        }

        // Populate top kernel colours
        int topColoursCount = 0;
        for (const auto& currentColour : colourCount.CountMapReversed) {            
            if (++topColoursCount <= clusterCenters) {
                
                std::array<float,3> centroids = { (float)getRedColour(currentColour.second),
                    (float)getGreenColour(currentColour.second),
                    (float)getBlueColour(currentColour.second)};
                
                colourCount.PreviousCluster.insert(std::make_pair(currentColour.second, centroids));
                colourCount.CurrentCluster.insert(std::make_pair(currentColour.second, centroids));
                
                if (DEBUG)
                    cout << "Centroids: " << get<1>(centroids) << endl;
            } else
                break;            
        }

    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }
    
    return colourCount;
}

/**
 * For a given pixel and the current list of cluster centers, the minimum distance
 * of that pixel is being found. This pixel is then being assigned/allocated to
 * that cluster center.
 */
void allocateToClusterTbb(uint32_t pixel, map<uint32_t, std::array<float,3>>& clusterCenters, 
        map<uint32_t, vector<uint32_t>> &pixelClusterAllocation) {   
    try {
        // TODO: Parallelize here        
        map<float, uint32_t> distancesMap;
        
        const int pixelRed = getRedColour(pixel);
        const int pixelGreen = getGreenColour(pixel);
        const int pixelBlue = getBlueColour(pixel);        
        
        // find all distances of the pixel from all the cluster centers
        for (const auto& it : clusterCenters) {
            uint32_t currentKernel = it.first;
                        
            const int currentKernelRed = getRedColour(currentKernel);
            const int currentKernelGreen = getGreenColour(currentKernel);
            const int currentKernelBlue = getBlueColour(currentKernel);
            
            // calculate distances
            float currentDistance = (float)sqrt(pow((currentKernelRed - pixelRed), 2) + 
                pow((currentKernelGreen - pixelGreen), 2) + 
                pow((currentKernelBlue - pixelBlue), 2));
                        
            distancesMap.insert(std::make_pair(currentDistance, currentKernel));
        }
   
        // get the minimum distance - The [0] element of the map has
        // the smallest distance by default, due to the nature of map data structures in c++ 
        uint32_t minDistanceKernel;
        for (const auto& currentColour : distancesMap) {
            minDistanceKernel = currentColour.second;
            break;            
        }

        // allocate that pixel to the closest cluster center        
        if (pixelClusterAllocation.find(minDistanceKernel)!= pixelClusterAllocation.end()) {
            pixelClusterAllocation[minDistanceKernel].push_back(pixel);
        } else {
            vector<uint32_t> newPixelVector;
            newPixelVector.push_back(pixel);
            pixelClusterAllocation.insert(std::make_pair(minDistanceKernel, newPixelVector));
        }        
        
    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }    
}

/*
 * Calculate cluster centroids
 */
void CalculateClusterCentroidsTbb(ColourCount& colourCount) {     
        
    for (const auto& currentCluster : colourCount.CurrentCluster) {        
                
        vector<uint32_t> colourList = colourCount.PixelClusterAllocation[currentCluster.first];
        
        float centroidRed = 0.0;
        float centroidGreen = 0.0;
        float centroidBlue = 0.0;
                
        for (std::vector<uint32_t>::iterator it = colourList.begin() ; it != colourList.end(); ++it) {
            
            uint32_t currentPixel = *it;

            std::array<float,3> currentClusterArray = colourCount.CurrentCluster.find(currentPixel)->second;
            
            centroidRed += get<0>(currentClusterArray);
            centroidGreen += get<1>(currentClusterArray);
            centroidBlue += get<2>(currentClusterArray);
                        
            map<uint32_t, uint32_t>::const_iterator it2 = colourCount.ClusterColours.find(currentPixel);            
            if (it2 == colourCount.ClusterColours.end()) {
                // Added to the cluster colours                
                colourCount.ClusterColours.insert(std::make_pair(currentPixel, getRGBHex(
                    centroidRed, centroidGreen, centroidBlue)));                        
            }
            
        }
        
        float count = colourList.size() + 1;
        
        // Average for the new centroid
        std::array<float,3> currentClusterArray = colourCount.CurrentCluster.find(currentCluster.first)->second;
        std::array<float,3> newCentroid = {(get<0>(currentClusterArray) + centroidRed) / count,
            (get<1>(currentClusterArray) + centroidGreen) / count,
            (get<2>(currentClusterArray) + centroidBlue) / count};
                
        // Store the new centroid value
        colourCount.CurrentCluster.find(currentCluster.first)-> second = newCentroid;        
    }
}

/**
 * Get Random points/cluster centers
 * @return 
 */
std::vector<std::array<uint32_t,2>> getRandomClusterCentersTbb(int clusterSize, int maxHeight, int maxWidth) {
    
    vector<std::array<uint32_t,2>> randomPointsVector;    
    uint32_t x = 0;
    uint32_t y = 0;
    
    // Initialize random seed
    srand (time(NULL));    

    // Generate a number between the pixel dimensions     
    parallel_for(0, clusterSize, [&](size_t i) {
        x = rand() % maxHeight;
        y = rand() % maxWidth;
        randomPointsVector.push_back({x, y});
    });
    
    return randomPointsVector;
}

ImageRGB segmentImageTbb(const ImageRGB& inputImage, ColourCount& colourCount) {
    try {
        ImageRGB imgMixedChannels;
        imgMixedChannels.width = inputImage.width;
        imgMixedChannels.height = inputImage.height;
        
        const int height = imgMixedChannels.height;
        const int width = imgMixedChannels.width;
        
        for (size_t i = 0; i < height; ++i) {    
            for (size_t j = 0; j < width; ++j) {
                const uint8_t r = get<0>(inputImage.data[i * width + j]);
                const uint8_t g = get<1>(inputImage.data[i * width + j]);
                const uint8_t b = get<2>(inputImage.data[i * width + j]);
                
                const uint32_t hexColour = getRGBHex(r, g, b); 
                
                // look up the float of the latest cluster
                //0. uint32_t newColour = colourCount.ClusterColours[hexColour];
                //map<uint32_t, vector<uint32_t>> PixelClusterAllocation;
                // 1. colourCount.PixelClusterAllocation
                // 2. CurrentCluster
                // 3. uint32_t newColour = getRGBHex(get<0>(colourCount.CurrentCluster[hexColour]), 
                //        get<1>(colourCount.CurrentCluster[hexColour]), get<2>(colourCount.CurrentCluster[hexColour]));
                
                //map<uint32_t, std::array<float,3>> CurrentCluster;
                
                uint32_t newColour = colourCount.ClusterColours[hexColour];
                
                // find the closest kernel of the current colour and replace it...
                const int pixelRed = getRedColour(hexColour);
                const int pixelGreen = getGreenColour(hexColour);
                const int pixelBlue = getBlueColour(hexColour);  
                map<float, uint32_t> distancesMap;      

                // find all distances of the pixel from all the cluster centers
                for (const auto& it : colourCount.CurrentCluster) {
                    uint32_t currentKernel = it.first;

                    int currentKernelRed = getRedColour(currentKernel);
                    int currentKernelGreen = getGreenColour(currentKernel);
                    int currentKernelBlue = getBlueColour(currentKernel);

                    // calculate distances
                    float currentDistance = (float)sqrt(pow((currentKernelRed - pixelRed), 2) + 
                        pow((currentKernelGreen - pixelGreen), 2) + 
                        pow((currentKernelBlue - pixelBlue), 2));

                    distancesMap.insert(std::make_pair(currentDistance, currentKernel));
                }

                // get the minimum distance - The [0] element of the map has
                // the smallest distance by default, due to the nature of map data structures in c++ 
                uint32_t minDistanceKernel;
                for (const auto& currentColour : distancesMap) {
                    minDistanceKernel = currentColour.second;
                    break;            
                }
                newColour = minDistanceKernel;
                
                if (DEBUG) {
                    cout << "colourCount.CurrentCluster size: " << colourCount.CurrentCluster.size() << endl;
                    cout << "colourCount.ClusterColours size: " << colourCount.ClusterColours.size() << endl;
                    cout << "Hexcolour: " << hexColour << " newColour: " << newColour << endl;
                }                    
                
                // replace the pixel colour                
                imgMixedChannels.data.push_back({(uint8_t)getRedColour(newColour),
                        (uint8_t)getGreenColour(newColour), (uint8_t)getBlueColour(newColour)});                    
            }
        }
                
        return imgMixedChannels;
        
    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }
}

/**
 * Returns a dithered image
 */
ImageRGB ditherImageTbb(const ImageRGB& inputImage) {
    ImageRGB imgMixedChannels = inputImage;
    try {
        
        const int height = imgMixedChannels.height;
        const int width = imgMixedChannels.width;
        
        parallel_for(0, height, [&](size_t i) {
            for (size_t j = 0; j < inputImage.width; ++j) {
                
                const uint8_t r = get<0>(imgMixedChannels.data[i * width + j]);
                const uint8_t g = get<1>(imgMixedChannels.data[i * width + j]);
                const uint8_t b = get<2>(imgMixedChannels.data[i * width + j]);
                           
                const uint32_t newColour = findClosestPaletteColour(r, g, b);
                              
                const uint32_t quantizationErrorRed = r - getRedColour(newColour);
                const uint32_t quantizationErrorGreen = g - getGreenColour(newColour);
                const uint32_t quantizationErrorBlue = b - getBlueColour(newColour);
                                
                // replace the current pixel colour                
                imgMixedChannels.data[i * width + j] = {getRedColour(newColour),
                        getGreenColour(newColour), getBlueColour(newColour)};     
                
                // pixel[x+1][y  ] := pixel[x+1][y  ] + quant_error * 7/16
                if ((i + 1) < height) {
                    imgMixedChannels.data[(i + 1) * width + j] =
                        { (uint8_t)(get<0>(imgMixedChannels.data[(i + 1) * width + j]) + ((7 * quantizationErrorRed) >> 4)), 
                        (uint8_t)(get<1>(imgMixedChannels.data[(i + 1) * width + j]) + ((7 * quantizationErrorGreen) >> 4)),
                        (uint8_t)(get<2>(imgMixedChannels.data[(i + 1) * width + j]) + ((7 * quantizationErrorBlue) >> 4)) };
                }
                                
                // pixel[x-1][y+1] := pixel[x-1][y+1] + quant_error * 3/16
                if ((i - 1) >= 0 && (j + 1) < width && ((i - 1) * width + (j + 1)) <
                        (width * height) && ((i - 1) * width + (j + 1)) > 0) {
                    
                    imgMixedChannels.data[(i - 1) * width + (j + 1)] =
                        { (uint8_t)(get<0>(imgMixedChannels.data[(i - 1) * width + (j + 1)]) + ((3 * quantizationErrorRed) >> 4)), 
                        (uint8_t)(get<1>(imgMixedChannels.data[(i - 1) * width + (j + 1)]) + ((3 * quantizationErrorGreen) >> 4)),
                        (uint8_t)(get<2>(imgMixedChannels.data[(i - 1) * width + (j + 1)]) + ((3 * quantizationErrorBlue) >> 4)) };
                }
                
                // pixel[x  ][y+1] := pixel[x  ][y+1] + quant_error * 5/16
                if ((j + 1) < width) {
                    
                    imgMixedChannels.data[i * width + (j + 1)] =
                        { (uint8_t)(get<0>(imgMixedChannels.data[i * width + (j + 1)]) + ((5 * quantizationErrorRed) >> 4)), 
                        (uint8_t)(get<1>(imgMixedChannels.data[i * width + (j + 1)]) + ((5 * quantizationErrorGreen) >> 4)),
                        (uint8_t)(get<2>(imgMixedChannels.data[i * width + (j + 1)]) + ((5 * quantizationErrorBlue) >> 4)) };
                }
                
                // pixel[x+1][y+1] := pixel[x+1][y+1] + quant_error * 1/16
                if ((i + 1) < height && (j + 1) < width) {
                    
                    imgMixedChannels.data[(i + 1) * width + (j + 1)] =
                        { (uint8_t)(get<0>(imgMixedChannels.data[(i + 1) * width + (j + 1)]) + ((1 * quantizationErrorRed) >> 4)), 
                        (uint8_t)(get<1>(imgMixedChannels.data[(i + 1) * width + (j + 1)]) + ((1 * quantizationErrorGreen) >> 4)),
                        (uint8_t)(get<2>(imgMixedChannels.data[(i + 1) * width + (j + 1)]) + ((1 * quantizationErrorBlue) >> 4)) };
                }                      
            }
        });            
        
    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }
    return imgMixedChannels;
}

/**
 * Perfoms a k-means iteration in the rgb colour space
 */
void iterateTbb(const ImageRGB& inputImage, ColourCount& colourCount) {
    
    try {        
        const int height = inputImage.height;
        const int width = inputImage.width;
                
        // for each pixel in the image
        for (size_t i = 0; i < height; ++i) {
            for (size_t j = 0; j < width; ++j) {
                
                const uint8_t r = get<0>(inputImage.data[i * width + j]);
                const uint8_t g = get<1>(inputImage.data[i * width + j]);
                const uint8_t b = get<2>(inputImage.data[i * width + j]);
               
                const uint32_t hexColour = getRGBHex(r, g, b);                                

                // Allocate pixel to cluster
                allocateToClusterTbb(hexColour, colourCount.CurrentCluster, colourCount.PixelClusterAllocation);
            }
        }
        
        // Calculate cluster centroids
        CalculateClusterCentroidsTbb(colourCount);

    } catch (exception & e) {
        cout << "Error: " << e.what() << endl;
    }    
}

#endif	/* UTIL_TBB_H */

